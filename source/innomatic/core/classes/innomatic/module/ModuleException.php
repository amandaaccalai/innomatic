<?php

/**
 * Exception thrown by Module objects and other Module classes.
 *
 * @author Alex Pagnoni <alex.pagnoni@innoteam.it>
 * @copyright Copyright 2004-2013 Innoteam Srl
 * @since 5.1
 */
class ModuleException extends RuntimeException
{
}
