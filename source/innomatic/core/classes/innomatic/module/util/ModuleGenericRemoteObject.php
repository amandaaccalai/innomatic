<?php

require_once('innomatic/module/ModuleRemoteObject.php');

/**
 * A generic remote object, with no custom client methods.
 *
 * @author Alex Pagnoni <alex.pagnoni@innoteam.it>
 * @copyright Copyright 2004-2013 Innoteam Srl
 * @since 5.1
 */
class ModuleGenericRemoteObject extends ModuleRemoteObject
{
}
